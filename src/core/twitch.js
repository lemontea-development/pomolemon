const tmi = require('tmi.js')
const twitchConfig = require('../config/TwitchConfig.json')
const client = new tmi.client(twitchConfig.tmi)
const eventHub = require('../eventHub')

module.exports.init = () => {
  console.log('initializing twitch module...')

  client.connect()
  client.on('message', onMessageHandler)
  client.on('connected', onConnectedHandler)

  const sessionUsers = [] // we need to temp store ids of users who adds task in order to delete them, unless we want to hook up to the twitch API to find it

  class ForestCode {
    constructor (code = '', duration = 45, state = false) {
      this.code = this.checkCode(code)
      this.duration = this.checkDuration(duration)
      this.state = state
    }

    checkCode (code) {
      return code.includes('--') ? '' : code
    }

    checkDuration (duration) {
      return duration.toString().includes('--') ? 40 : duration
    }
  }
  
  let fc = new ForestCode()

  // Called every time a message comes in
  function onMessageHandler (target, context, msg, self) {
    if (self || !msg.startsWith('!')) return // checks if the message was intended as a command

    const args = msg.slice(1).split(' ')
    const command = args.shift().toLowerCase() // transforms the command to lowercase without the !, the ! check is already being done in the first if of this scope
    commandHandler(command, args, context, target)
  }

  function commandHandler(command, args, context, target) {
    let commandName = command
    const minutes = args[0]
    const message = (isNumeric(args[0]) ? args.slice(1) : args.slice(0) ).join(' ')

    // maybe refactor this to seperate files in the future? maybe
    switch (command) {
      case 'manifest':
        const task = {
          user: {
            id: context['user-id'],
            username: context.username
          },
          title: message,
          duration: addMinutes(minutes)
        }
        if (sessionUsers.filter(user => user.id === context['user-id']).length === 0) {
          sessionUsers.push({
            id: context['user-id'],
            name: context.username
          })
        }
        eventHub.emit('added_task', task)
        client.say(target, `you can do it ${context.username}!! annale1Hype annale1Love`)
        console.log(`* Executed ${commandName} command`)
      break;
      
      case 'fc':
        const fcCode = args[0]
        const fcDuration = args[2]

        if (args.includes('--set') && !args.includes('--setduration')) { // sets the forest code
          if (!(/^\d+$/.test(fcCode))) {
            fc.code = fcCode
            fc.duration = /^\d+$/.test(fcDuration) ? fcDuration : fc.duration
            fc.state = true
            console.log(`PomopLemon=>Executed ${command} with set attribute`)
          } else {
            console.log(`PomopLemon=>Executed ${command} failed, invalid value for the FC code`)
          }
        }
  
        if (args.includes('--clear')) { // clears the forest code
          fc = new ForestCode() // resets to a blank object
          console.log(`PomopLemon=>Executed ${command} with clear attribute`)
        }
  
        if (args.includes('--setduration')) {
          fc.duration = args[1]
          client.say(target, `The pomodoro duration has been set to ${args[1]} minutes.`)
        }
  
        if (fc.state) {
          client.say(target, `Time to put down your phone and get back to work! Enter my room code: ${fc.code} to plant a ${fc.duration}-minute tree with me! You can also tap on this link to join me: https://www.forestapp.cc/join-room?token=${fc.code}`)
        } else if (!args.includes('--')) {
          client.say(target, 'There is no Forest room active at the moment.')
        }
      break;
      
      case 'done':
        eventHub.emit('remove_task', context['user-id'])
        client.say(target, `Good job @${context.username} ! You have manifested a lot of tasks today annale1Love annale1Hype`)
      break;
  
      case 'remove':
        if (context.mod || context.username === 'annalemontea') {
          const obj = args[0].replace('@', '').toLowerCase() // cleans up user input to onlyi contain the username
          const userId = getUserIdFromSession(obj)
          if (userId !== false) {
            eventHub.emit('remove_task', userId)
            console.log(`* command [mod]${command} was run with argument of ${args}`)
          } else {
            console.log(`* no tasks available for ${args[0]}`)
          }
        } else {
          console.log(`* command [mod]${command} requires mod privileges`)
        }
      break;
  
      case 'addtime':
        const amount = args[0]
        eventHub.emit('added_time', context['user-id'], amount)
        client.say(target, `added ${amount} minutes to your task @${context.username}`)
      break;

      default:
        break;
    }
  }

  function addMinutes(minutes) {
    return minutes? new Date((+new Date) + minutes*60000).getTime() : null
  }

function isNumeric(val) {
  return /^-?\d+$/.test(val);
}


  function getUserIdFromSession (username) {
    const obj = sessionUsers.filter(user => user.name === username)
    if (obj.length > 0) {
      return obj[0].id
    }
    return false
  }
  // Called every time the bot connects to Twitch chat
  function onConnectedHandler (addr, port) {
    console.log(`* Connected to ${addr}:${port}`)
  }
}
