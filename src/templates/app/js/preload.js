const { getWindowButtonLayout } = require('window-button-layout')
const { ipcRenderer } = require("electron");

const windowLayout = getWindowButtonLayout()

let minutes = 5;

const dummyTask = {
    user: {
        id: 'uuidu29030f000f0ddd',
        username: 'nevethebish69'
    },
    title: 'overtake the lemontea channel',
    duration: new Date((+new Date) + minutes*60000).getTime() //unix timestamp (minutes difference)
}

ipcRenderer.on('update-downloaded', ()=> {
    document.querySelector('update').style.display = 'block';
})

window.addEventListener('DOMContentLoaded', () => {
    // sets nav layout
    const nav = document.querySelector('nav')
    const start = windowLayout.start.toString().replace(/,/g, ' ');
    const end = windowLayout.end.toString().replace(/,/g, ' ');
    nav.style.gridTemplateAreas = `'${start} ${end}'`;

    // nav button actions
    document.querySelector('.close').addEventListener("click", () => {
        ipcRenderer.send('close-me')
    })

    document.querySelector('.maximize').addEventListener("click", () => {
        ipcRenderer.send('maximize')
    })

    document.querySelector('.minimize').addEventListener("click", () => {
        ipcRenderer.send('minimize')
    })

    document.getElementById('clearlist').addEventListener('click', () => {
        ipcRenderer.send('clear-list-command')
    })
    document.getElementById('add-dummy-task').addEventListener('click', () => {
        ipcRenderer.send('added_task', dummyTask)
    })
    document.getElementById('update').addEventListener('click', () => {
        ipcRenderer.send('restart_app')
    })

});