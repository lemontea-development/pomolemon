const socket = io()
const list = document.getElementById('list')

// #region methods
const generateTaskString = (task, duration) => {
  if (duration) {
    return `<li id="${task.user.id}"> ${duration} min &#9829;  ${task.user.username} - ${task.title}</li>\n`
  }
  return `<li id="${task.user.id}">&#9829;  ${task.user.username} - ${task.title}</li>\n`
}
const setTask = task => {
  list.innerHTML += generateTaskString(task)
  tasks.push(task)
  sessionStorage.setItem('tasks', JSON.stringify(tasks))
}

const removeTask = (task) => {
  tasks = tasks.filter(_task => _task.user.id !== task.user.id)
  sessionStorage.setItem('tasks', JSON.stringify(tasks))    
  document.getElementById(task.user.id).remove()
}
// #endregion

// initializes the tasks
let tasks = sessionStorage.getItem('tasks') ? JSON.parse(sessionStorage.getItem('tasks')) : []
for (let task = 0; task < tasks.length; task++) {
  list.innerHTML += list.innerHTML += generateTaskString(tasks[[task]])
}

// #region socket actions
socket.on('added task', task => {
  // if new user
  if (tasks && tasks.filter(_task => _task.user.id === task.user.id).length === 0) {
    setTask(task)
  } else {
    removeTask(task)
    setTask(task)
  }
})

socket.on('clear list', () => {
  tasks = []
  list.innerHTML = ''
  sessionStorage.removeItem('tasks')
})

socket.on('remove task', userId => {
  console.log(userId);
  const filteredTask = tasks.filter(_task => _task.user.id === userId)[0]
  removeTask(filteredTask)
})

socket.on('add time', (userId, amount) => {
  let filteredTask = tasks.filter(_task => _task.user.id === userId)[0]
  filteredTask.duration = addMinutes(amount, filteredTask.duration)
  removeTask(filteredTask)
  setTask(filteredTask)
})
// #endregion

//#region timer
const timerLimit = 240 // minutes
setInterval(() => {
  let currentTimestamp = new Date().getTime();
  let sortedTasks = tasks.sort((dateA, dateB) => dateA.duration - dateB.duration)

  for (let task = 0; task < sortedTasks.length; task++) {
    if(sortedTasks[task].duration)
    {
      let timeLeft = (sortedTasks[task].duration - currentTimestamp)
      let difference = Math.floor((timeLeft % (1000 * 60 * timerLimit)) / (1000 * 60));
      // update board times
      document.getElementById(sortedTasks[task].user.id).remove()
      list.innerHTML += generateTaskString(sortedTasks[task], (difference + 1))

      if (difference < 0) {
        removeTask(sortedTasks[task])
      }
    }
  }
}, 1000);
//#endregion

// #region scroll check
const resizeEvent = event => {
  if (list.clientHeight > event.srcElement.outerHeight) {
    if (!list.getAttribute('scroll')) {
      list.setAttribute('scroll', '')
    }
  } else {
    list.removeAttribute('scroll')
  }
}
window.addEventListener('resize', resizeEvent)
list.addEventListener('resize', resizeEvent)
// #endregion

function addMinutes(minutes, currentTimestamp) {
  if (currentTimestamp) {
    return minutes? new Date(currentTimestamp + minutes*60000).getTime() : null
  }
  return minutes? new Date((+new Date) + minutes*60000).getTime() : null
}