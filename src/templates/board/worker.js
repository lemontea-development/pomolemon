// express board http server
const electron = require('electron');
const path = require('path');
const { ipcRenderer } = require('electron')

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

const port = 3000
app.use(express.static(__dirname))

//#region socket messaging
ipcRenderer.on('test message', (event, message) => {
  console.log(message)
})

// #region developer emits only
ipcRenderer.on('added_task_emit', (event, task) => {
  io.emit('added task', task)
})
ipcRenderer.on('remove_task', (event, userId) => {
  io.emit('remove task', userId)
})
ipcRenderer.on('add_time', (event, userId, amount) => {
  io.emit('add time', userId, amount)
})

ipcRenderer.on('clear-list', () => {
  io.emit('clear list')
})

app.get('/', (request, result) => {
    result.sendFile(path.join(__dirname, 'index.html'))
})

server.listen(port)
