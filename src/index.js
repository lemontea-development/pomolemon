const { app, BrowserWindow } = require('electron');
const path = require('path');
const { ipcMain } = require('electron');
const { autoUpdater } = require('electron-updater')
const log = require('electron-log')

const gitConfig = require('./config/gitConfig.json')
const eventHub = require('./eventHub')
const twitch = require('./core/twitch')

let mainWindow, boardWindow;

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}


const createWindow = () => {

  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      preload: path.join(__dirname, './templates/app/js/preload.js')
    },
    icon: path.join(__dirname, './resources/appicon.png'),
    frame: false
  });

  boardWindow = new BrowserWindow({
    show: false,
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInWorker: true,
      preload: path.join(__dirname, './templates/board/worker.js')
    }
  });
  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, './templates/app/index.html'));
  
  boardWindow.loadFile(path.join(__dirname, './templates/board/index.html'));
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
// dev response: but that sounds like work and we don't do that here

  twitch.init();

  //#region custom window actions
  ipcMain.on('close-me', () => {
    app.quit()
  })

  ipcMain.on('maximize', () => {
    let maximized = mainWindow.isMaximized();
    maximized ? mainWindow.unmaximize() : mainWindow.maximize()
  })

  ipcMain.on('minimize', event => {
    mainWindow.minimize();
  })
  //#endregion

  ipcMain.on('app_version', (event) => {
    event.sender.send('app_version', { version: app.getVersion() });
  });
  
  ipcMain.on('added_task', (event, task) => {
    sendAddTask(task);
  })

  ipcMain.on('clear-list-command', (event) => {
    boardWindow.webContents.send("clear-list")
  })

  // eventhub events
  eventHub.on('added_task', (task) => {
    console.log(task)
    sendAddTask(task)
  })

  eventHub.on('remove_task', (userId) => {
    boardWindow.webContents.send('remove_task', userId)
  })

  eventHub.on('added_time', (userId, amount) => {
    boardWindow.webContents.send('add_time', userId, amount)
  })


  function sendAddTask(task) {
    boardWindow.webContents.send('added_task_emit', task)
  }

  //#endregion

  //#region updater
  autoUpdater.requestHeaders = { 'PRIVATE-TOKEN': 'glpat-R4_NKdF6A6RKy-hFsyNT' }
  autoUpdater.autoDownload = true
  autoUpdater.logger = log
  autoUpdater.logger.transports.file.level = 'info'

  autoUpdater.on('update-available', () => {
    sendStatusToWindow('An Update is available....')
    mainWindow.webContents.send('update_available')
  })

  autoUpdater.on('update-downloaded', () => {
    sendStatusToWindow('Update has been downloaded....')
    mainWindow.webContents.send('update_downloaded')
  })

  ipcMain.on('restart_app', () => {
    sendStatusToWindow('In onRestart_App')
    autoUpdater.quitAndInstall()
  })

  autoUpdater.on('checking-for-update', () => {
    sendStatusToWindow('Checking for update...')
  })

  autoUpdater.on('update-not-available', function (info) {
    sendStatusToWindow('Update not available.')
  })

  autoUpdater.on('error', function (err) {
    sendStatusToWindow('We have an error in auto-updater: ')
    sendStatusToWindow(String(err))
  })

  autoUpdater.on('download-progress', progressObj => {
    let log_message = 'Download speed: ' + progressObj.bytesPerSecond
    log_message =
      log_message + ' - Downloaded ' + parseInt(progressObj.percent) + '%'
    log_message =
      log_message + ' (' + progressObj.transferred + '/' + progressObj.total + ')'
    sendStatusToWindow(log_message)
  })

  // Check for an update 10sec after Program Starts
  setTimeout(() => {
    sendStatusToWindow('We are checking for updates and notifying user...')
    autoUpdater.checkForUpdatesAndNotify()
  }, 10000)

  // Check for an update every 2min.
  setInterval(() => {
    sendStatusToWindow('We are checking for updates and notifying user...')
    autoUpdater.checkForUpdatesAndNotify()
  }, 120000)

  function sendStatusToWindow(message) {
    log.info(message)
  }
  //#endregion