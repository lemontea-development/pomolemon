# PomoLemon



#Libraries we use
socket.io

eslint

electron


eslint autoformatter settings
```
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "eslint.validate": ["javascript"]
```


```
https://id.twitch.tv/oauth2/authorize?response_type=code&client_id=vn1gl5l39ra28j71675dx9vxuhd0kq&redirect_uri=http://localhost&scope=viewing_activity_read%20chat:read%20chat:edit
```
https://twitchapps.com/tmi/

keeping code in honor of our raving korean

```
const config = require('config')
const tmi = require('tmi.js')

const options = {
  options: {
    debug: true
  },
  connection: {
    cluster: 'aws',
    reconnect: true
  },
  identity: {
    username: config.get('auth.username'),
    password: config.get('auth.oauth')
  },
  channels: config.get('channels')
}

const client = new tmi.client(options)

const initPomoLemonBot = () => {
  client.connect()

  const pomoBoard = []

  client.on('message', (channel, user, message, self) => {
    if (self) { return }

    const command = message.trim().split(' ')[0]

    if (command === '!pomo') {
      const pomoDuration = message.split(' ')[1]
      const pomoTask = message.split(' ')[2]
      const userName = user['display-name']
      console.log(addToPomoBoard(userName, pomoDuration, pomoTask))
      console.log(`PomoLemon=>executed ${command} command`)
    } else {
      console.log(`PomoLemon=>unknown command ${command}`)
    }
  })

  function addToPomoBoard (user, duration, task) {
    const taskObj = new pomoTask(user, duration, task)

    pomoBoard.push(taskObj)
    console.log(pomoBoard)
    return `Total of ${pomoBoard.length} on the board`
  }

  var pomoTask = function (user, duration, task) {
    this.user = user
    this.duration = duration
    this.task = task
    this.timestamp = Date.now()
  }

  const taskCount = function () {
    return pomoBoard.length
  }

  function onConnectedHandler (addr, port) {
    console.log(`* Connected to ${addr}:${port}`)
  }
}

initPomoLemonBot()

```

very cool!

#why functional programming?
because classes are bad for bussiness (italian joke), The real answer: performance. classes aren't really performance friendly in js yet.

how to build
1.
```sh npm2yarn
npm install --save-dev @electron-forge/cli
npx electron-forge import

✔ Checking your system
✔ Initializing Git Repository
✔ Writing modified package.json file
✔ Installing dependencies
✔ Writing modified package.json file
✔ Fixing .gitignore

We have ATTEMPTED to convert your app to be in a format that electron-forge understands.

Thanks for using "electron-forge"!!!
```


2.
```
npm run make

> my-electron-app@1.0.0 make /my-electron-app
> electron-forge make

✔ Checking your system
✔ Resolving Forge Config
We need to package your application before we can make it
✔ Preparing to Package Application for arch: x64
✔ Preparing native dependencies
✔ Packaging Application
Making for the following targets: zip
✔ Making for target: zip - On platform: darwin - For arch: x64
```

